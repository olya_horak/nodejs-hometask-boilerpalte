const { user } = require('../models/user');
const { UserRepository } = require('../repositories/userRepository');


const createUserValid = (req, res, next) => {
    // Implement validatior for user entity during creation
    const reqUser = req.body;
    const { email, phoneNumber, password } = reqUser;

    const required = ["firstName", "lastName", "email", "phoneNumber", "password"];

    required.map(field => {
        if (!reqUser[field]) {
            res.err = `${field} is required`
        }
    })

    if (!res.err) {

        if ((password).length < 3) {
            res.err = "Password should have at least 3 symbols";
        }
        if (!String(phoneNumber).startsWith("+380")) {
            res.err = "Phone number should be +380*********";
        }
        if (String(phoneNumber).length !== 13) {
            res.err = "Phone number should be +380*********";
        }
        if (!String(email).endsWith("@gmail.com")) {
            res.err = "Email should be @gmail.com";
        }

        const emailInUse = UserRepository.exists(
            (dbUser) => {
                return dbUser.email.toUpperCase() === reqUser.email.toUpperCase();
            }
        );
        if (emailInUse) {
            res.err = "User with such email already exists";
        }

        const phoneInUse = UserRepository.exists(
            (dbUser) => {
                return dbUser.phoneNumber === reqUser.phoneNumber;
            }
        );
        if (phoneInUse) {
            res.err = "User with such phone number already exists";
        }
    }

    next();
}

const updateUserValid = (req, res, next) => {
    // Implement validatior for user entity during update
    const reqUser = req.body;
    const { email, phoneNumber, password } = reqUser;

    const requiredSome = ["firstName", "lastName", "email", "phoneNumber", "password"];

    const hasSome = requiredSome.some(field => {
        return reqUser[field]
    });
    if (!hasSome) {
        res.err = "Nothing to be updated";
    }

    if (!res.err) {

        if ((password).length < 3) {
            res.err = "Password should have at least 3 symbols";
        }

        if (reqUser.email !== undefined) {
            if (!String(email).endsWith("@gmail.com")) {
                res.err = "Email should be @gmail.com";
            }
            const emailInUse = UserRepository.exists(
                (dbUser) => {
                    return dbUser.email.toUpperCase() === reqUser.email.toUpperCase();
                },
                req.params.id
            );
            if (emailInUse) {
                res.err = "User with such email already exists";
            }
        }

        if (reqUser.phoneNumber !== undefined) {
            if (!String(phoneNumber).startsWith("+380")) {
                res.err = "Phone number should be +380*********";
            }
            if (String(phoneNumber).length !== 13) {
                res.err = "Phone number should be +380*********";
            }
            const phoneInUse = UserRepository.exists(
                (dbUser) => {
                    return dbUser.phoneNumber === reqUser.phoneNumber;
                }, req.params.id
            );
            if (phoneInUse) {
                res.err = "User with such phone number already exists";
            }
        }
    }

    next();
}

exports.createUserValid = createUserValid;
exports.updateUserValid = updateUserValid;